
![](logo.png)


**“PROYECTO DE ANALISIS DE SISTEMAS”**

---

***sistemas para solicitud de citas de referencias en SIS***

---
**Hospital Nacional Arzobispo Loayza** 
**PROFESOR: RICHARD LEONARDO BERROCAL NAVARRO**

**ORCID: 0000-0001-8718-3150**
` `**EQUIPO NRO. 1**

**INTEGRANTES**

Carlos Melecio(ORCID)

Jeferson Nikelson(ORCID)

Sancho Diaz,Erika Yanela(0000-0002-4467-1346)

Brandon Baltazar La Torre(0000-0002-8806-8696)

Condori Condori, William Antony (0000-0001-5538-0627)

Soto Trejo Cristel Jazmin (0000-0001-7485-1113)

Leonardo Douglas(ORCID)

LIMA- 2022

RESUMEN EJECUTIVO.

INTRODUCCIÓN

***1.ESTUDIO DE FACTIBILIDAD***

1.1. Factibilidad operativa y técnica: La visión del sistema***

***2.MODELO DEL NEGOCIO***

2.1. Modelo de Caso de Uso del Negocio

2.1.1. Lista de los Actores del Negocio

2.1.2. Lista de Casos de Uso del Negocio

2.1.3. Diagrama de Casos de Uso del Negocio

2.1.4. Especificaciones de Casos de uso del Negocio

2.2. Modelo de Análisis del Negocio

2.2.1. Lista de trabajadores de negocio

2.2.2. Lista de entidades de negocio

2.2.3. Realización de Casos de Uso del Negocio

2.2.4. Diagrama de actividades

2.2.5. Realización de clases de dominio

2.3. Glosario de términos

2.4. Reglas de negocio
#
- **DATOS DE LA EMPRESA.**

>El hospital Arzobispo Loayza, cuenta con una serie de servicios en línea al servicio de  población tales como; búsqueda de resultado de laboratorio, resultado de diagnóstico de imágenes, el tarifario por especialidad y el libro de reclamaciones; cabe resaltar que cuenta con 71 médicos de 21 especialidades que se han inscrito voluntariamente para participar, teniendo en cuenta la coyuntura actual en la que nuestros pacientes no pueden acudir a los establecimientos de salud”, precisó el doctor Machicado, quien agregó que las especialidades que estarán brindando este servicio son: Cardiología, Cirugía General, Cirugía Vascular Periférica, Angiología, Endocrinología, Hematología, Gastroenterología, Epidemiología, Inmunohematología, Medicina Interna, Nefrología, Neurocirugía, Obstetricia, Oftalmología, Otorrinolaringología, Pediatría, Toxicología, Traumatología, Unidad de Cuidados Intensivo y Urología.

>En el  hospital las atenciones se iniciarán a partir de las 8:00 am y en varios casos se extenderán hasta las 6:00 pm, facilitando a los pacientes el acceso a este servicio.
#
- **Reseña Histórica.**

>Fue fundado por el Primer Arzobispo del Perú y de América, Don Jerónimo de Loayza y Gonzáles, en 1549. Este hospital fue dedicado exclusivamente a prestar servicios de salud a la población indígena, diezmada y severamente afectada por las diversas enfermedades traídas por los españoles a nuestro país. Cabe resaltar que hasta entonces los indígenas no se les consideraba como seres humanos, por tanto, no tenían acceso a los servicios de salud. El Arzobispo Loayza falleció el 26 de octubre de 1575, siendo enterrado, según su deseo, en la iglesia del hospital. Con el transcurso del tiempo el hospital de Santa Ana fue dedicado posteriormente a la atención de mujeres menesterosas.
En 1902 ante el deterioro de sus instalaciones, la Beneficencia Pública de Lima, decidió construir un moderno hospital para mujeres, en unos terrenos de su propiedad en la Avenida Alfonso Ugarte, por lo que el 27 de enero de 1905 se emitió la Resolución Suprema aprobatoria para su construcción y así continuar con la obra del Arzobispo.
La construcción de este nosocomio se inició en 1915 y fue inaugurado el 11 de diciembre de 1924, bajo el nombre de Hospital Arzobispo Loayza, en honor a su fundador. El personal y equipo del hospital Santa Ana fue trasladado al nuevo hospital.
Desde su inauguración, nuestro hospital estuvo dedicado a la atención de mujeres de escasos recursos económicos, lo que se mantuvo hasta mediados de los 90, actualmente atiende tanto a pacientes de ambos sexos. Hasta el 31 de enero de 1974, en que pasó a depender del Ministerio de Salud, fue administrado por la Sociedad de Beneficencia Pública de Lima.

>El Hospital Loayza es el hospital general más grande del país, heredero de una noble tradición de servicio, con un gran prestigio, bien ganado, en el campo médico, que continúa siendo referente para el resto de instituciones de salud de nuestro país y manteniendo la mística de su fundador, desarrolla una política acorde con la del sector; brindando las facilidades para la atención oportuna de gran cantidad de personas de escasos recursos económicos, que no cuentan con ningún tipo de seguro y deben acudir a los hospitales del Estado, para recuperar, en la medida de lo posible, uno de sus bienes más preciados: la salud.
#
- **Documentos de gestión (manuales, directivas, organigramas, etc).**

**MANUALES:**

>![](manual.png)
#
**PROCESOS:**

>![](Procesos1.png)

>![](Procesos2.png)

>![](Procesos3.png)
#
**ORGANIGRAMAS:**

![](Organigrama.png)
#
- **Descripción del Negocio**

>Para solicitar la cita de pacientes que cuentan con seguro SIS el Hospital Loayza recibe las referencias que son ingresadas por el personal de salud de las postas  a la plataforma web, posteriormente esta referencia es verificada por el medio auditor del Hospital Loayza, para de esta manera validar que los datos del paciente, el diagnóstico, la anamnesis y especialidad esté de acuerdo con el tratamiento del paciente. Si la referencia es observada ya sea por el mal llenado del diagnóstico o error de especialidad a la cual se solicita la referencia, el personal de la posta de donde es solicitada la referencia realiza modificaciones según la descripción de la observación la cual son registradas por el médico auditor del Hospital Loayza. Ya con la observación subsanada el personal de Admisión Genera ya cita según la fecha más próxima .El médico auditor informa al personal de la posta la fecha de la cita para que de esta manera el paciente se pueda acercar al Hospital Loayza a atenderse.
#
- **Diagrama Eriksson-Penker**

>![](DiagramaErikPenker.png)
#
- **Diagrama de Actividades**
>![](DiagramaAct.png)
#
- **Diagrama de Clases**

>![](DiagramaClases.png)